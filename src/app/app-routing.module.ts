import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './pages/employee/create/create.component';
import { EditComponent } from './pages/employee/edit/edit.component';
import { IndexComponent } from './pages/employee/index/index.component';

const routes: Routes = [
  // { path: '', redirectTo: 'employee/index', component: IndexComponent },
  { path: 'employee/index', component: IndexComponent },
  { path: 'employee/create', component: CreateComponent },
  { path: 'employee/edit', component: EditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
